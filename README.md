## Test work of simulating a league table 
In order to run the project, please do the following:
 - set up .env file 
 - run composer install
 - run php artisan migrate
 - run php artisan db:seed
 - run php artisan serve and on your terminal you will see a host to app. Just click it and run the project

In case you have simulated the whole tournament and want to begin from scratch, just press button _"Begin from scratch"_ 

I know that this task could have been written way better. In real task I would use more privileges of Laravel
