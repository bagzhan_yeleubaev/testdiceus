<?php

use App\Http\Controllers\StandingsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [StandingsController::class, 'index'])->name('index');
Route::get('nextWeek', [StandingsController::class, 'nextWeek']);
Route::get('currentWeek', [StandingsController::class, 'currentWeek']);
Route::get('playAll', [StandingsController::class, 'playAll']);
Route::get('scratch', [StandingsController::class, 'scratch']);

