<?php

namespace Database\Seeders;

use App\Models\Standings;
use Illuminate\Database\Seeder;

class StandingsInitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['team' => 1, 'points' => 0, 'played' => 0, 'wins' => 0, 'draws' => 0, 'loses' => 0, 'goalDiff' => 0],
            ['team' => 2, 'points' => 0, 'played' => 0, 'wins' => 0, 'draws' => 0, 'loses' => 0, 'goalDiff' => 0],
            ['team' => 3, 'points' => 0, 'played' => 0, 'wins' => 0, 'draws' => 0, 'loses' => 0, 'goalDiff' => 0],
            ['team' => 4, 'points' => 0, 'played' => 0, 'wins' => 0, 'draws' => 0, 'loses' => 0, 'goalDiff' => 0]
        ];
        Standings::insert($data);
    }
}
