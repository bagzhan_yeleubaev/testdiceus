<?php

namespace Database\Seeders;

use App\Models\Schedule;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['week' => 1, 'teamA' => 1, 'teamB' => 2],
            ['week' => 1, 'teamA' => 3, 'teamB' => 4],
            ['week' => 2, 'teamA' => 1, 'teamB' => 3],
            ['week' => 2, 'teamA' => 2, 'teamB' => 4],
            ['week' => 3, 'teamA' => 1, 'teamB' => 4],
            ['week' => 3, 'teamA' => 2, 'teamB' => 3],
            ['week' => 4, 'teamA' => 3, 'teamB' => 2],
            ['week' => 4, 'teamA' => 4, 'teamB' => 1],
            ['week' => 5, 'teamA' => 4, 'teamB' => 2],
            ['week' => 5, 'teamA' => 3, 'teamB' => 1],
            ['week' => 6, 'teamA' => 4, 'teamB' => 3],
            ['week' => 6, 'teamA' => 2, 'teamB' => 1],
        ];
        Schedule::insert($data);

    }
}
