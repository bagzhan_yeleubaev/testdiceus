<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
          ['name' => 'Manchester City', 'strength' => 55],
          ['name' => 'Arsenal', 'strength' => 33],
          ['name' => 'Liverpool', 'strength' => 51],
          ['name' => 'Chelsea', 'strength' => 49],
        ];
        Team::insert($teams);
    }
}
