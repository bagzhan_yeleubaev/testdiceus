<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->id();
            $table->integer('week');
            $table->unsignedBigInteger('teamA');
            $table->unsignedBigInteger('teamB');
            $table->unsignedBigInteger('match')->nullable();
            $table->foreign('teamA')->references('id')->on('teams');
            $table->foreign('teamB')->references('id')->on('teams');
            $table->foreign('match')->references('id')->on('matches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
