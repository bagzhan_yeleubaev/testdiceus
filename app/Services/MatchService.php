<?php

namespace App\Services;

use App\Models\Team;

class MatchService
{
    const ATTACK_ATTEMPS_AMOUNT_LIMIT = 35;

    private Team $teamA, $teamB;
    private array $teams;


    /**
     * @param Team $teamA
     * @param Team $teamB
     */
    public function __construct(Team $teamA, Team $teamB)
    {
        $this->teamA = $teamA;
        $this->teamB = $teamB;
        $this->favoriteByStrength();
    }

    /**
     * @return array
     */
    public function simulateMatch()
    {
        for ($i = 0; $i < self::ATTACK_ATTEMPS_AMOUNT_LIMIT; $i++) {
            $rand = rand(1, 100);
            if ($rand < $this->teams[0]->strength) {
                $this->teams[0]->attack($this->teams[1]->strength);
            } elseif ($rand > $this->teams[1]->strength) {
                $this->teams[1]->attack($this->teams[0]->strength);
            }
        }
        $this->setStandings($this->teamA->getScore(), $this->teamB->getScore());

        return [$this->teamA->getScore(), $this->teamB->getScore()];
    }

    /**
     *
     * @return void
     */
    private function favoriteByStrength()
    {
        if ($this->teamA->strength > $this->teamB->strength) {
            $this->teams[0] = $this->teamA;
            $this->teams[1] = $this->teamB;
        } else {
            $this->teams[0] = $this->teamB;
            $this->teams[1] = $this->teamA;
        }
    }

    /**
     * @param int $teamARes
     * @param int $teamBRes
     */
    private function setStandings(int $teamARes, int $teamBRes)
    {
        $standingA = $this->teamA->standing;
        $standingA->goalDiff += $teamARes - $teamBRes;
        $standingA->played += 1;

        $standingB = $this->teamB->standing;
        $standingB->goalDiff += $teamBRes - $teamARes;
        $standingB->played += 1;

        if ($teamARes > $teamBRes) {
            $standingA->points += 3;
            $standingA->wins += 1;
            $standingB->loses += 1;
        } elseif ($teamARes < $teamBRes) {
            $standingB->points += 3;
            $standingB->wins += 1;
            $standingA->loses += 1;
        } else {
            $standingA->points += 1;
            $standingB->points += 1;
            $standingA->draws += 1;
            $standingB->draws += 1;
        }
        $standingA->save();
        $standingB->save();
    }
}
