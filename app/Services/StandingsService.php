<?php

namespace App\Services;

use Illuminate\Support\Facades\Artisan;
use App\Repositories\ScheduleRepository;
use App\Repositories\StandingsRepository;

class StandingsService
{

    private StandingsRepository $standingsRepository;
    private ScheduleRepository $scheduleRepository;

    /**
     * @param StandingsRepository $standingsRepository
     * @param ScheduleRepository $scheduleRepository
     */
    public function __construct(StandingsRepository $standingsRepository, ScheduleRepository $scheduleRepository)
    {
        $this->standingsRepository = $standingsRepository;
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * @return array
     */
    public function getNextWeek(): array
    {
        $lastPlayed = $this->standingsRepository->getLastPlayed();
        $schedule = $this->scheduleRepository->getLastSchedule($lastPlayed);
        $scheduleService = new ScheduleService($schedule);
        $scheduleService->playMatches();
        $lastPlayed += 1;
        $matches = $this->scheduleRepository->getCurrentMatches($lastPlayed);
        $standings = $this->standingsRepository->getStandings();

        return compact('standings', 'matches', 'lastPlayed');
    }

    /**
     * @return array
     */
    public function getCurrentWeek(): array
    {
        $lastPlayed = $this->standingsRepository->getLastPlayed();
        $standings = $this->standingsRepository->getStandings();
        $matches = $this->scheduleRepository->getCurrentMatches($lastPlayed);

        return compact('standings', 'matches', 'lastPlayed');
    }

    /**
     * @return array
     */
    public function generateAllMatches(): array
    {
        $schedule = $this->scheduleRepository->getAllMatches();
        $scheduleService = new ScheduleService($schedule);
        $scheduleService->playMatches();
        $lastPlayed = $this->standingsRepository->getLastPlayed();
        $standings = $this->standingsRepository->getStandings();
        $matches = $this->scheduleRepository->getCurrentMatches($lastPlayed);

        return compact('standings', 'matches', 'lastPlayed');
    }

    /**
     * @return array
     */
    public function beginFromScratch(): array
    {
        Artisan::call('migrate:fresh --seed');
        $lastPlayed = $this->standingsRepository->getLastPlayed();
        $standings = $this->standingsRepository->getStandings();
        $matches = $this->scheduleRepository->getCurrentMatches($lastPlayed);

        return compact('standings', 'matches', 'lastPlayed');
    }

}
