<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use App\Repositories\MatchRepository;
use Illuminate\Support\Facades\App;

class ScheduleService
{

    private Collection $schedule;

    /**
     * @param Collection $schedule
     */
    public function __construct(Collection $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     *
     * @return boolean
     */
    public function playMatches()
    {
        foreach ($this->schedule as $teams) {
            $matchService = new MatchService($teams->getTeamA, $teams->getTeamB);
            $res = $matchService->simulateMatch();
            $matchRepository = App::make(MatchRepository::class);
            $matchId = $matchRepository->createMatch($res);
            $teams->match = $matchId;
            $teams->save();
        }

        return true;
    }
}
