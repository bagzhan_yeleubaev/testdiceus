<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';
    public $timestamps = false;
    private int $res = 0;
    protected $fillable = ['name', 'strength'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schedule()
    {
        return $this->hasMany(Schedule::class, 'teamA', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function standing()
    {
        return $this->belongsTo(Standings::class, 'id', 'team');
    }

    /**
     * @param $opponentStrength
     * @return void
     */
    public function attack($opponentStrength)
    {
        $rand = rand(1, 100);
        $chance = abs($this->strength - $opponentStrength);
        if ($rand < $chance) {
            $this->score();
        }
    }

    /**
     *
     * @return void
     */
    private function score()
    {
        $this->res += 1;
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->res;
    }
}
