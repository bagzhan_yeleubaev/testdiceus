<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Standings extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['team', 'points', 'played', 'wins', 'draws', 'loses', 'goalDiff'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function teamName()
    {
        return $this->hasOne(Team::class, 'id');
    }

}
