<?php

namespace App\Repositories;

use App\Models\Match;

class MatchRepository
{
    private Match $match;

    /**
     * @param Match $match
     */
    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    /**
     * @param array $result
     * @return mixed
     */
    public function createMatch(array $result)
    {
        $match = $this->match->create([
            'teamARes' => $result[0],
            'teamBRes' => $result[1]
        ]);

        return $match->id;
    }

}
