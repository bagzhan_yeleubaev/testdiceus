<?php

namespace App\Repositories;

use App\Models\Schedule;

class ScheduleRepository
{
    private Schedule $schedule;

    /**
     * @param Schedule $schedule
     */
    public function __construct(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * @param int $lastPlayed
     * @return mixed
     */
    public function getLastSchedule(int $lastPlayed)
    {
        return $this->schedule->where('week', $lastPlayed + 1)->get();
    }

    /**
     * @param int $lastPlayed
     * @return mixed
     */
    public function getCurrentMatches(int $lastPlayed)
    {
        return $this->schedule->where('week', $lastPlayed)
            ->where('match', '!=', null)
            ->get();
    }

    /**
     * @return mixed
     */
    public function getAllMatches()
    {
        return $this->schedule->where('match', null)->get();
    }
}
