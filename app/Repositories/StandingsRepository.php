<?php

namespace App\Repositories;

use App\Models\Standings;

class StandingsRepository
{

    private Standings $standings;

    /**
     * @param Standings $standings
     */
    public function __construct(Standings $standings)
    {
        $this->standings = $standings;
    }

    /**
     * @return mixed
     */
    public function getLastPlayed()
    {
        return $this->standings->max('played');
    }

    /**
     * @return mixed
     */
    public function getStandings()
    {
        return $this->standings->orderBy('points', 'desc')
            ->orderBy('goalDiff', 'desc')
            ->get();
    }

}
