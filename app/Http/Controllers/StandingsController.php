<?php

namespace App\Http\Controllers;

use App\Services\StandingsService;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\View\View as ContractView;

class StandingsController extends Controller
{

    private StandingsService $standingsService;

    /**
     * @param StandingsService $standingsService
     */
    public function __construct(StandingsService $standingsService)
    {
        $this->standingsService = $standingsService;
    }

    public function index()
    {
        return view('welcome');
    }

    /**
     * @return ContractView
     */
    public function nextWeek(): ContractView
    {
        return View::make('standingsTable', $this->standingsService->getNextWeek());
    }

    /**
     * @return ContractView
     */
    public function currentWeek(): ContractView
    {
        return View::make('standingsTable', $this->standingsService->getCurrentWeek());
    }

    /**
     * @return ContractView
     */
    public function playAll(): ContractView
    {
        return View::make('standingsTable', $this->standingsService->generateAllMatches());
    }

    /**
     * @return ContractView
     */
    public function scratch(): ContractView
    {
        return View::make('standingsTable', $this->standingsService->beginFromScratch());
    }
}
