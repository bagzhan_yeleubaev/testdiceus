<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>League Standings</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .csh_12 {
            background: red;
        }
    </style>
</head>
<body class="antialiased">
<div class="row" id="standings">

</div>
<div class="row">
    <div class="col d-flex justify-content-center">
        <button type="button" class="btn btn-primary btn-sm" onclick="playAll()">Play All</button>
    </div>
    <div class="col d-flex justify-content-center">
        <button type="button" class="btn btn-info btn-sm" onclick="nextWeek()">Next Week</button>
    </div>
</div>
<div class="row">
    <div class="col d-flex justify-content-center">
        <button type="button" class="btn btn-warning btn-sm" onclick="scratch()">Begin from scratch</button>
    </div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    function nextWeek() {
        $('#standings').load('nextWeek');
    }

    function playAll() {
        $('#standings').load('playAll');
    }

    function scratch() {
        $('#standings').load('scratch');
    }

    $(document).ready(function () {
        $('#standings').load('currentWeek');
    });
</script>
</html>
