<div class="col">
    <div>
        <div class="col d-flex justify-content-center">
            <h3>League Table</h3>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Teams</th>
                <th scope="col">PTS</th>
                <th scope="col">P</th>
                <th scope="col">W</th>
                <th scope="col">D</th>
                <th scope="col">L</th>
                <th scope="col">GD</th>
            </tr>
            </thead>
            <tbody>
            @foreach($standings as $current)
                <tr>
                    <th>{{ $current->teamName->name }}</th>
                    <td>{{ $current->points }}</td>
                    <td>{{ $current->played }}</td>
                    <td>{{ $current->wins }}</td>
                    <td>{{ $current->draws }}</td>
                    <td>{{ $current->loses }}</td>
                    <td>{{ $current->goalDiff }}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


</div>
<div class="col">
    <div class="col d-flex justify-content-center">
        <h3>Match Results</h3>
    </div>
    @if(sizeof($matches) > 0)
        <h5 class="col d-flex justify-content-center"> {{$lastPlayed}} Week Match Result</h5>
        @foreach($matches as $match)
            <div class="row">
                <div class="col">
                    <p>{{$match->getteamA->name}}</p>
                </div>
                <div class="col">
                    <p>{{$match->getMatch->teamAres}}-{{$match->getMatch->teamBres}}</p>
                </div>
                <div class="col">
                    <p>{{$match->getteamB->name}}</p>
                </div>
            </div>
        @endforeach
    @else
        <div class="row">
            No matches played yet!
        </div>
    @endif

</div>
